package TabliceIPetle.Zadanie34;

import java.util.Scanner;

public class MainZadanie34 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNextLine()) {
            System.out.println("Podaj komende i slowo");
            String linia = sc.nextLine();

            // dziele linie ;
            String[] slowa = linia.split(" ", 2);
            String slowoDoZakodowania = slowa[1]; // dowolne slowo

            String kod = slowa[0];
            try {
                int przesuniecie = Integer.parseInt(kod);
                // udało nam sie sparsować liczbe

                for (int i = 0; i < slowoDoZakodowania.length(); i++) {
                    char znak = slowoDoZakodowania.charAt(i);
                    char przesuniety = (char) (znak + przesuniecie);
                    System.out.print(przesuniety);
                }

            } catch (NumberFormatException nfe) {
                continue;
            }


        }
    }
}
