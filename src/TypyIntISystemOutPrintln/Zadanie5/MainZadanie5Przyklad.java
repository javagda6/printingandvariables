package TypyIntISystemOutPrintln.Zadanie5;

public class MainZadanie5Przyklad {
    public static void main(String[] args) {
        /*
        Zadanie 5: Zadeklaruj dwie zmienne - 'waga' oraz 'wzrost'.
        Przypisz do nich jakieś wartości. Stwórz instrukcję warunkową
        ('if') który sprawdza czy osoba (np. taka która wchodzi na
        kolejkę/rollercoaster) jest wyższa niż 150 cm wzrostu i
        nie przekracza wagą 180 kg.

        Zadanie 5 a: Dopisz do poprzedniej aplikacji dodatkową zmienną - wiek. Jeśli osoba jest młodsza niż 10 lat, lub starsza niż 80, to nie może wejść na kolejkę.
          b: Dopisz/zmień do/w poprzedniej aplikacji - osoba może wejść na kolejkę jeśli jej wzrost jest od 150 do 220 cm wzrostu.
          c: Dopisz deskryptywne wyjasnienia. Jesli osoba nie moze wejsc na kolejke, to wypisz na konsole odpowiedni komunikat dlaczego. np.
                    Jesli osoba nie moze wejsc z powodu wagi, to powinien sie wypisac komunikat ze nie moze wejsc bo przekracza limit wagowy
                    Jesli osoba nie moze wejsc z powodu wieku, to powinien sie wypisac INNY komunikat o tym ze nie moze wejsc z powodu wieku.
         */
        int wiek = 25;
        int waga = 80;
        int wzrost = 180;

        // 10 < wiek < 80
        // waga < 180
        // 220 > wzrost > 150

        if ((wiek >= 10) && (wiek <= 80) &&
                (waga <= 180) && (wzrost >= 150) && (wzrost <= 220)) {
            System.out.println("Wszystko ok, możesz wejść.");
        } else {
            if (wiek < 10) {
                System.out.println("Jesteś zbyt młody");
            }
            if (wiek > 80) {
                System.out.println("Jesteś zbyt stary");
            }
            if (waga > 180) {
                System.out.println("Jesteś zbyt ciężki");
            }
            if (wzrost < 150) {
                System.out.println("Jesteś zbyt niski");
            }
            if (wzrost > 220) {
                System.out.println("Jesteś zbyt wysoki.");
            }
        }

    }
}
