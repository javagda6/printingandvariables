package TabliceIPetle.Zadanie32;

import java.util.Scanner;

public class MainZadanie32 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String liniaTekstu = sc.nextLine();

        int suma = 0;
        for (int i = 0; i < liniaTekstu.length(); i++) {
            char litera = liniaTekstu.charAt(i);

            if (litera >= 48/* 0 */ && litera <= 57 /* 9 */) {
                System.out.println("Znak: " + litera + " ma wartosc= " + ((int) litera) + " a po odjeciu = " + (litera - 48));
                suma += (litera - 48);
            }
        }

        System.out.println("Wartość: " + suma);
    }
}
