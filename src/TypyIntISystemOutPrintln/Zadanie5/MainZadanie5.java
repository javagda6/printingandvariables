package TypyIntISystemOutPrintln.Zadanie5;

public class MainZadanie5 {
    public static void main(String[] args) {
        int wiek = 25;
        int waga = 80;
        int wzrost = 180;

        // 10 < wiek < 80
        // waga < 180
        // 220 > wzrost > 150

        boolean moze_wejsc = true;
        if (wiek < 10) {
            System.out.println("Jesteś zbyt młody");
            moze_wejsc = false;
        }
        if (wiek > 80) {
            System.out.println("Jesteś zbyt stary");
            moze_wejsc = false;
        }
        if (waga > 180) {
            System.out.println("Jesteś zbyt ciężki");
            moze_wejsc = false;
        }
        if (wzrost < 150) {
            System.out.println("Jesteś zbyt niski");
            moze_wejsc = false;
        }
        if (wzrost > 220) {
            System.out.println("Jesteś zbyt wysoki.");
            moze_wejsc = false;
        }

        if (moze_wejsc) {
            System.out.println("Wszystko ok, możesz wejść.");
        }else{
            System.out.println("Nie możesz wejść.");
        }
    }
}
