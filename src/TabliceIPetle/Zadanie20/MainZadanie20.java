package TabliceIPetle.Zadanie20;

import java.util.Random;
import java.util.Scanner;

public class MainZadanie20 {
    public static void main(String[] args) {
        Random random = new Random(100);
        int losowanaLiczba = random.nextInt();

        Scanner sc = new Scanner(System.in);

//        System.out.println("Podaj liczbe:");
//        int wpisanaLiczba = sc.nextInt();
//        while (wpisanaLiczba != losowanaLiczba) {
//            if (wpisanaLiczba < losowanaLiczba) {
//                System.out.println("Twoja liczba jest mniejsza");
//            } else if (wpisanaLiczba > losowanaLiczba) {
//                System.out.println("Twoja liczba jest wieksza");
//            }
//            System.out.println("Podaj liczbe:");
//            wpisanaLiczba = sc.nextInt();
//        }

        int wpisanaLiczba;
        do {
            System.out.println("Podaj liczbe:");
            wpisanaLiczba = sc.nextInt();
            if (wpisanaLiczba < losowanaLiczba) {
                System.out.println("Twoja liczba jest mniejsza");
            } else if (wpisanaLiczba > losowanaLiczba) {
                System.out.println("Twoja liczba jest wieksza");
            }
        } while (wpisanaLiczba != losowanaLiczba);

        System.out.println("Gratulacje, odgadłeś!");
    }
}
