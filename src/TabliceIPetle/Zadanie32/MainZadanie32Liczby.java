package TabliceIPetle.Zadanie32;

import java.util.Scanner;

public class MainZadanie32Liczby {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String linia = sc.nextLine();

        String[] slowa = linia.split(" ");

        int suma = 0;

        for (int i = 0; i < slowa.length; i++) {
            String slowo = slowa[i];

            try {
                int wartoscLiczbowaSlowa = Integer.parseInt(slowo);

                // jesli instrukcja u gory sie wykona, to wykonaja sie pozostale instrukcje bloku try
                System.out.println("Wartosc liczbowa: " + wartoscLiczbowaSlowa);
                suma += wartoscLiczbowaSlowa;
            } catch (NumberFormatException nfe) {
                // jesli parsowanie (Integer.parseInt) sie nie powiedzie to wypisujemy komunikat
                System.out.println("Slowo: " + slowo + " nie jest liczba.");
            }
        }
        System.out.println("Suma " + suma);

    }
}
