package TabliceIPetle.Zadanie18;

import java.util.Scanner;

public class MainZadanie18 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ilosc liczb");
        int iloscLiczb = scanner.nextInt();

        int[] tablicaLiczb = new int[iloscLiczb];

        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;

        double srednia = 0;

        for (int i = 0; i < iloscLiczb; i++) {
            System.out.println("Podaj " + i + " liczbę:");
            tablicaLiczb[i] = scanner.nextInt();

            if (max < tablicaLiczb[i]) {
                max = tablicaLiczb[i];
            }
            if (min > tablicaLiczb[i]) {
                min = tablicaLiczb[i];
            }

            srednia += tablicaLiczb[i];
        }
        System.out.println("Minimum : " + min);
        System.out.println("Maximum : " + max);
        System.out.println("Min+Max : " + (min + max));
        System.out.println("Srednia arytm.: " + (srednia / iloscLiczb));

//        int max = Integer.MIN_VALUE;
//        int min = Integer.MAX_VALUE;
//        int max = tablicaLiczb[0];
//        int min = tablicaLiczb[0];
//
//        for (int i = 0; i < iloscLiczb; i++) {
//            if (max < tablicaLiczb[i]) {
//                max = tablicaLiczb[i];
//            }
//            if (min > tablicaLiczb[i]) {
//                min = tablicaLiczb[i];
//            }
//        }

    }
}
