package ZadaniaZeScannerem.Zadanie9;

import java.util.Scanner;

public class Zadanie9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); //
        System.out.println("Podaj wiek:");
        int wiek = sc.nextInt();
        System.out.println("Podaj wage:");
        int waga = sc.nextInt();
        System.out.println("Podaj wzrost:");
        int wzrost = sc.nextInt();

        // 10 < wiek < 80
        // waga < 180
        // 220 > wzrost > 150

        boolean moze_wejsc = true;
        if (wiek < 10) {
            System.out.println("Jesteś zbyt młody");
            moze_wejsc = false;
        }
        if (wiek > 80) {
            System.out.println("Jesteś zbyt stary");
            moze_wejsc = false;
        }
        if (waga > 180) {
            System.out.println("Jesteś zbyt ciężki");
            moze_wejsc = false;
        }
        if (wzrost < 150) {
            System.out.println("Jesteś zbyt niski");
            moze_wejsc = false;
        }
        if (wzrost > 220) {
            System.out.println("Jesteś zbyt wysoki.");
            moze_wejsc = false;
        }

        if (moze_wejsc) {
            System.out.println("Wszystko ok, możesz wejść.");
        } else {
            System.out.println("Nie możesz wejść.");
        }
    }
}
