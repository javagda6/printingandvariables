package TabliceIPetle.Zadanie25;

import java.util.Random;

public class MainZadanie25 {
    public static void main(String[] args) {
        int[] tablica = new int[20];
        Random generator = new Random();

        for (int i = 0; i < 20; i++) {
            tablica[i] = generator.nextInt(10) + 1  ;
        }

        // tablica kubełkowa ( zliczanie do kubełków (komórka jest kubełkiem))
        // 10 elementowa tablica z wartościami 0
        int[] liczniki_liczb = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        for (int i = 0; i < 20; i++) {
            liczniki_liczb[tablica[i] - 1]++;
        }

        // wypisanie
        for (int i = 0; i < 10; i++) {
            System.out.println(i + " - " + liczniki_liczb[i]);
        }
    }
}
