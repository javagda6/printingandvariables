package TypyIntISystemOutPrintln.Zadanie3;

public class MainZadanie3 {
    public static void main(String[] args) {

        /*
        Zadanie 3: Zadeklaruj 3 zmienne - zmienna 'a',
        zmienna 'b', zmienna 'c'. Przypisz jej 3 wartości
        - ważne żeby były różne. Następnie wykonaj na nich
        następujące działania:
            a) przepisz wartości - do zmiennej 'a' przypisz
                wartość 'b', do zmiennej 'b' przypisz wartość 'c',
                do zmiennej 'c' przypisz wartość 'a'.
            b) wypisz wartości na ekran.
         */

        int a = 1;
        int b = 2;
        int c = 3;

        // opcja 1 :
        System.out.println("A = " + a + ", B = " + b + ", C = " + c);
        // opcja 2:
//        System.out.println("A = " + a);
//        System.out.println("B = " + b);
//        System.out.println("C = " + c);

        int zmienna_tymczasowa = a;
        a = b;
        b = c;
        c = zmienna_tymczasowa;

        System.out.println("A = " + a + ", B = " + b + ", C = " + c);

    }
}
